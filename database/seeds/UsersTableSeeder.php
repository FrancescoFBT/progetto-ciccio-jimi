<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addUser("Public Domain Pictures", "pgp@pgp.com", "twmbrV3GM7&PjD");
    }

    public function addUser($name, $email, $password)
    {
    	$user = new User();

    	$user->name = $name;
    	$user->email = $email;
    	$user->password = $password;
       
        $user->save();
    }
}
