<?php

use Illuminate\Database\Seeder;
use App\Image;
use App\User;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$this->addImage("http://www.publicdomainpictures.net/pictures/30000/velka/artistic-painter.jpg", "JPEG");
       	$this->addImage("http://www.publicdomainpictures.net/pictures/230000/velka/stained-glass-1499913127FLs.jpg", "JPEG");
       	$this->addImage("http://www.publicdomainpictures.net/pictures/40000/velka/child-artist.jpg", "JPEG");
       	$this->addImage("http://www.publicdomainpictures.net/pictures/50000/velka/artist-draws-a-picture.jpg", "JPEG");
       	$this->addImage("http://www.publicdomainpictures.net/pictures/10000/velka/2527-1274914706T7Ef.jpg", "JPEG");
    }

    public function addImage($src, $mimetype)
    {
    	$img = new Image();

        $img->user_id = User::find(1)->id;
        
        $img->src = $src;
        $img->mimetype = $mimetype;
        $img->size = 0;
        $img->save();
    }
}
