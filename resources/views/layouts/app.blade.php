<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    @include('layouts.components._meta')
    @stack('style')
    <body>
        <div id="app">
            @include('layouts.components._navbar')
            @yield('content')
            @include('layouts.components._footer')
        </div>
        <!-- Scripts -->
        @include('layouts.components._js')
        @stack('scripts')
    </body>
</html>
