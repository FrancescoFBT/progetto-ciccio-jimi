@extends('layouts.app')

@section('content')
	@if (Auth::guest())
		<div class="col-lg-8 col-md-8 hidden-sm hidden-xs">
			<img class="home-img" src="{{$image->src}}" alt="">
		</div>
		<div class="hidden-lg hidden-md col-sm-12 col-xs-12">
			<img class="home-img-sm" src="{{$image->src}}" alt="">
			<br>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<h1 class="text-center"><strong>ACCEDI</strong></h1>
			<form class="home-login" method="POST" action="{{ route('login') }}">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="email" class="control-label"><strong>E-Mail</strong></label>
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
				</div>
				<div class="form-group">
					<label for="password" class="control-label"><strong>Password</strong></label>
					<input id="password" type="password" class="form-control" name="password" required>
				</div>
				<div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Ricordami</label>
                    </div>
                </div>
                <div class="form-group group-center">
	                <button type="submit" class="btn btn-primary">Accedi</button>
					<a class="btn btn-primary" href="{{ route('password.request') }}">Password dimenticata?</a>
                </div>
			</form>
			<h2 class="text-center"><strong><a href="{{ route('register') }}">Non sei ancora un membro? Registrati subito adesso!</a></strong></h2>
		</div>
	@else
		<p>Loggato</p>
	@endif
@endsection
